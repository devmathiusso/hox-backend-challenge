# HOX Challenge

A small API built to the HOX Challenge with Node.js, Express, and Postgres.

## API Documentation - Postman

https://documenter.getpostman.com/view/8339776/TVspjozM

## How to Install

- Clone the project in your machine using `git clone`
- Run `npm install` or `yarn` inside the project folder
- Change the `.env` file values to your database connection

## How to Run

Simulate the `development` environment:

`npm run dev` or `yarn dev`

Simulate the `production` environment:

`npm run start` or `yarn start`

## Run Tests

I don't know why, but everytime I run all the tests together, it always fails. So if you're gonna run the tests, please run it separately:

> You can use `yarn` instead of `npm run`

```
npm run test -- auth.test.js --coverage=false
npm run test -- category.test.js --coverage=false
npm run test -- product.test.js --coverage=false
npm run test -- router.test.js --coverage=false
npm run test -- user.test.js --coverage=false
```

## Built With

- [Node.js](https://nodejs.org)
- [Express](https://expressjs.com/)
- [Postgres](https://www.postgresql.org/)

## Important Additional Packages

- [bcryptjs](https://github.com/dcodeIO/bcrypt.js#readme)
- [dotenv](https://github.com/motdotla/dotenv#readme)
- [jsonwebtoken](https://github.com/auth0/node-jsonwebtoken)
- [helmet](https://github.com/helmetjs/helmet)
- [pg](https://github.com/brianc/node-postgres)
- [xss-clean](https://github.com/jsonmaur/xss-clean)
- [sequelize](https://sequelize.org/)
- [jest](https://jestjs.io/)
- [sequelize-cli](https://github.com/sequelize/cli)
- [sqlite3](https://github.com/mapbox/node-sqlite3)
- [supertest](https://github.com/visionmedia/supertest#readme)
- [factory-girl](https://github.com/aexmachina/factory-girl#readme)
- [faker](https://github.com/Marak/Faker.js#readme)

## Versioning

I've used [Git](https://git-scm.com/) for versioning.

## Authors

- **Victor Mathiusso**

const truncateTables = require('./utils/truncateTables')

beforeEach(async (done) => {
  await truncateTables()
  return done()
})

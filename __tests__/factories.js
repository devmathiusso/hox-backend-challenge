const faker = require('faker')
const { factory } = require('factory-girl')
const { User, Category, Product } = require('../src/app/models')

factory.define('User', User, {
  name: faker.name.findName(),
  email: faker.internet.email(),
  password: faker.internet.password(),
})

factory.define('Category', Category, {
  name: 'Refrigerante',
})

factory.define('Product', Product, {
  name: 'Coca-Cola 2L',
  manufacturing_date: '2020-12-11 08:15',
  perishable_product: true,
  expiration_date: '2021-12-11 08:15',
  price: 9.5,
})

module.exports = factory

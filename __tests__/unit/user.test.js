const bcrypt = require('bcryptjs')
const { User } = require('../../src/app/models')
const truncateTables = require('../utils/truncateTables')

describe('User', () => {
  beforeEach(async () => {
    await truncateTables()
  })

  it('should encrypt user password', async () => {
    const user = await User.create({
      name: 'Victor Mathiusso',
      email: 'dev.mathiusso@gmail.com',
      password: '123456',
    })

    const compareHash = await bcrypt.compare('123456', user.password_hash)

    expect(compareHash).toBe(true)
  })
})

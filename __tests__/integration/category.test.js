const request = require('supertest')
const app = require('../../src/app')
const factory = require('../factories')
const { Category, Product } = require('../../src/app/models')

const routePrefix = '/api/v1/categories'

describe('Categories', () => {
  it('should create category', async () => {
    const user = await factory.create('User')
    const response = await request(app)
      .post(`${routePrefix}`)
      .send({ name: 'Refrigerante' })
      .set('Authorization', `Bearer ${user.generateToken()}`)

    expect(response.status).toBe(201)
    expect(response.body).toHaveProperty('category')
  })

  it('should not create category with missing fields', async () => {
    const user = await factory.create('User')
    const response = await request(app)
      .post(`${routePrefix}`)
      .set('Authorization', `Bearer ${user.generateToken()}`)

    expect(response.status).toBe(400)
  })

  it('should update category', async () => {
    const user = await factory.create('User')
    const category = await factory.create('Category')

    const response = await request(app)
      .patch(`${routePrefix}/${category.id}`)
      .send({ name: 'Refrigerante Editado' })
      .set('Authorization', `Bearer ${user.generateToken()}`)

    expect(response.status).toBe(200)
    expect(response.body).toHaveProperty('category')
    expect(response.body.category.name).toBe('Refrigerante Editado')
  })

  it('should not update category with missing fields', async () => {
    const user = await factory.create('User')
    const category = await factory.create('Category')

    const response = await request(app)
      .patch(`${routePrefix}/${category.id}`)
      .send({ name: '' })
      .set('Authorization', `Bearer ${user.generateToken()}`)

    expect(response.status).toBe(400)
  })

  it('should not update category with an invalid ID', async () => {
    const user = await factory.create('User')
    const response = await request(app)
      .patch(`${routePrefix}/987654321`)
      .send({ name: 'Refrigerante Editado' })
      .set('Authorization', `Bearer ${user.generateToken()}`)

    expect(response.status).toBe(404)
  })

  it('should delete category', async () => {
    const user = await factory.create('User')
    let category = await factory.create('Category')

    const response = await request(app)
      .delete(`${routePrefix}/${category.id}`)
      .set('Authorization', `Bearer ${user.generateToken()}`)

    category = await Category.findByPk(category.id)

    expect(response.status).toBe(204)
    expect(category).toBe(null)
  })

  it('should not delete category with an invalid ID', async () => {
    const user = await factory.create('User')
    const response = await request(app)
      .delete(`${routePrefix}/987654321`)
      .set('Authorization', `Bearer ${user.generateToken()}`)

    expect(response.status).toBe(404)
  })

  it('should delete products attached to the deleted category', async () => {
    const user = await factory.create('User')
    const category = await factory.create('Category')

    await factory.create('Product', { category_id: category.id })

    await request(app)
      .delete(`${routePrefix}/${category.id}`)
      .set('Authorization', `Bearer ${user.generateToken()}`)

    const products = await Product.findAll({
      where: { category_id: category.id },
    })

    expect(products.length).toBe(0)
  })

  it('should fetch one category by ID', async () => {
    const user = await factory.create('User')
    const category = await factory.create('Category')

    const response = await request(app)
      .get(`${routePrefix}/${category.id}`)
      .set('Authorization', `Bearer ${user.generateToken()}`)

    expect(response.status).toBe(200)
    expect(response.body).toHaveProperty('category')
  })

  it('should not fetch one category with an invalid ID', async () => {
    const user = await factory.create('User')
    const response = await request(app)
      .get(`${routePrefix}/987654321`)
      .set('Authorization', `Bearer ${user.generateToken()}`)

    expect(response.status).toBe(404)
  })

  it('should fetch categories', async () => {
    const user = await factory.create('User')
    await factory.create('Category')
    await factory.create('Category')

    const response = await request(app)
      .get(`${routePrefix}`)
      .set('Authorization', `Bearer ${user.generateToken()}`)

    expect(response.status).toBe(200)
    expect(response.body).toHaveProperty('categories')
    expect(response.body.categories.length).toBe(2)
  })
})

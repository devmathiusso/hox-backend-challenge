const request = require('supertest')
const app = require('../../src/app')
const factory = require('../factories')
const { Product } = require('../../src/app/models')

const routePrefix = '/api/v1/products'
const defaultProduct = {
  name: 'Coca-Cola 2L',
  manufacturing_date: '2020-12-11 08:15',
  perishable_product: true,
  expiration_date: '2021-12-11 08:15',
  price: 9.5,
}

describe('Products', () => {
  it('should create product', async () => {
    const user = await factory.create('User')
    const category = await factory.create('Category')
    const response = await request(app)
      .post(`${routePrefix}`)
      .send({
        ...defaultProduct,
        category_id: category.id,
      })
      .set('Authorization', `Bearer ${user.generateToken()}`)

    expect(response.status).toBe(201)
    expect(response.body).toHaveProperty('product')
  })

  it('should not create a product if the category does not exist', async () => {
    const user = await factory.create('User')
    const response = await request(app)
      .post(`${routePrefix}`)
      .send({
        ...defaultProduct,
        category_id: 987654321,
      })
      .set('Authorization', `Bearer ${user.generateToken()}`)

    expect(response.status).toBe(404)
  })

  it('should not create a product with missing fields', async () => {
    const user = await factory.create('User')
    const category = await factory.create('Category')
    const response = await request(app)
      .post(`${routePrefix}`)
      .send({
        ...defaultProduct,
        name: '',
        category_id: category.id,
      })
      .set('Authorization', `Bearer ${user.generateToken()}`)

    expect(response.status).toBe(400)
  })

  it('should not create a product with an invalid manufacturing date', async () => {
    const user = await factory.create('User')
    const category = await factory.create('Category')
    const response = await request(app)
      .post(`${routePrefix}`)
      .send({
        ...defaultProduct,
        manufacturing_date: 'invalid date',
        category_id: category.id,
      })
      .set('Authorization', `Bearer ${user.generateToken()}`)

    expect(response.status).toBe(400)
  })

  it('should not create a product with an invalid expiration date', async () => {
    const user = await factory.create('User')
    const category = await factory.create('Category')
    const response = await request(app)
      .post(`${routePrefix}`)
      .send({
        ...defaultProduct,
        expiration_date: 'invalid date',
        category_id: category.id,
      })
      .set('Authorization', `Bearer ${user.generateToken()}`)

    expect(response.status).toBe(400)
  })

  it('should not create a product if the manufacturing date is greater than the expiration date', async () => {
    const user = await factory.create('User')
    const category = await factory.create('Category')
    const response = await request(app)
      .post(`${routePrefix}`)
      .send({
        ...defaultProduct,
        manufacturing_date: '2020-12-11 13:30:01',
        expiration_date: '2020-12-11 13:30:00',
        category_id: category.id,
      })
      .set('Authorization', `Bearer ${user.generateToken()}`)

    expect(response.status).toBe(400)
  })

  it('should update product', async () => {
    const user = await factory.create('User')
    const category = await factory.create('Category')
    const category2 = await factory.create('Category', { name: 'Eletrônicos' })
    const product = await factory.create('Product', {
      category_id: category.id,
    })

    const response = await request(app)
      .patch(`${routePrefix}/${product.id}`)
      .send({
        ...defaultProduct,
        name: 'Coca-Cola 2L Editada',
        category_id: category2.id,
      })
      .set('Authorization', `Bearer ${user.generateToken()}`)

    expect(response.status).toBe(200)
    expect(response.body).toHaveProperty('product')
    expect(response.body.product.name).toBe('Coca-Cola 2L Editada')
    expect(response.body.product.category_id).not.toBe(category.id)
  })

  it('should not update a product with an invalid ID', async () => {
    const user = await factory.create('User')
    const response = await request(app)
      .patch(`${routePrefix}/987654321`)
      .set('Authorization', `Bearer ${user.generateToken()}`)

    expect(response.status).toBe(404)
  })

  it('should not update a product if the category does not exist', async () => {
    const user = await factory.create('User')
    const category = await factory.create('Category')
    const product = await factory.create('Product', {
      category_id: category.id,
    })

    const response = await request(app)
      .patch(`${routePrefix}/${product.id}`)
      .send({
        ...defaultProduct,
        category_id: 987654321,
      })
      .set('Authorization', `Bearer ${user.generateToken()}`)

    expect(response.status).toBe(404)
  })

  it('should not update a product with missing fields', async () => {
    const user = await factory.create('User')
    const category = await factory.create('Category')
    const product = await factory.create('Product', {
      category_id: category.id,
    })

    const response = await request(app)
      .patch(`${routePrefix}/${product.id}`)
      .send({
        ...defaultProduct,
        name: '',
      })
      .set('Authorization', `Bearer ${user.generateToken()}`)

    expect(response.status).toBe(400)
  })

  it('should not update a product with an invalid manufacturing date', async () => {
    const user = await factory.create('User')
    const category = await factory.create('Category')
    const product = await factory.create('Product', {
      category_id: category.id,
    })

    const response = await request(app)
      .patch(`${routePrefix}/${product.id}`)
      .send({
        ...defaultProduct,
        manufacturing_date: 'invalid date',
        category_id: category.id,
      })
      .set('Authorization', `Bearer ${user.generateToken()}`)

    expect(response.status).toBe(400)
  })

  it('should not update a product with an invalid expiration date', async () => {
    const user = await factory.create('User')
    const category = await factory.create('Category')
    const product = await factory.create('Product', {
      category_id: category.id,
    })

    const response = await request(app)
      .patch(`${routePrefix}/${product.id}`)
      .send({
        ...defaultProduct,
        expiration_date: 'invalid date',
        category_id: category.id,
      })
      .set('Authorization', `Bearer ${user.generateToken()}`)

    expect(response.status).toBe(400)
  })

  it('should not update a product if the manufacturing date is greater than the expiration date', async () => {
    const user = await factory.create('User')
    const category = await factory.create('Category')
    const product = await factory.create('Product', {
      category_id: category.id,
    })

    const response = await request(app)
      .patch(`${routePrefix}/${product.id}`)
      .send({
        ...defaultProduct,
        manufacturing_date: '2020-12-11 13:30:01',
        expiration_date: '2020-12-11 13:30:00',
        category_id: category.id,
      })
      .set('Authorization', `Bearer ${user.generateToken()}`)

    expect(response.status).toBe(400)
  })

  it('should delete product', async () => {
    const user = await factory.create('User')
    const category = await factory.create('Category')
    let product = await factory.create('Product', { category_id: category.id })

    const response = await request(app)
      .delete(`${routePrefix}/${product.id}`)
      .set('Authorization', `Bearer ${user.generateToken()}`)

    product = await Product.findByPk(product.id)

    expect(response.status).toBe(204)
    expect(product).toBe(null)
  })

  it('should not delete product with an invalid ID', async () => {
    const user = await factory.create('User')
    const response = await request(app)
      .delete(`${routePrefix}/987654321`)
      .set('Authorization', `Bearer ${user.generateToken()}`)

    expect(response.status).toBe(404)
  })

  it('should fetch one product by ID', async () => {
    const user = await factory.create('User')
    const category = await factory.create('Category')
    const product = await factory.create('Product', {
      category_id: category.id,
    })

    const response = await request(app)
      .get(`${routePrefix}/${product.id}`)
      .set('Authorization', `Bearer ${user.generateToken()}`)

    expect(response.status).toBe(200)
    expect(response.body).toHaveProperty('product')
  })

  it('should not fetch one product with an invalid ID', async () => {
    const user = await factory.create('User')
    const response = await request(app)
      .get(`${routePrefix}/987654321`)
      .set('Authorization', `Bearer ${user.generateToken()}`)

    expect(response.status).toBe(404)
  })
})

const request = require('supertest')
const app = require('../../src/app')

describe('Routes', () => {
  it('should receive an error on try to access an undefined route', async () => {
    const response = await request(app).get(`/undefined-route`)
    expect(response.status).toBe(404)
  })

  it('should not be able to access private routes without JWT token', async () => {
    const response = await request(app).get('/api/v1/categories')
    expect(response.status).toBe(401)
  })
})

const request = require('supertest')
const app = require('../../src/app')
const factory = require('../factories')

const routePrefix = '/api/v1/auth'

describe('Authentication', () => {
  it('should create user with valid credentials', async () => {
    const response = await request(app).post(`${routePrefix}/register`).send({
      name: 'Victor Mathiusso',
      email: 'dev.mathiusso@gmail.com',
      password: '123456',
    })

    expect(response.status).toBe(201)
    expect(response.body).toHaveProperty('user')
  })

  it('should not create user with missing fields', async () => {
    const response = await request(app).post(`${routePrefix}/register`).send({
      email: 'dev.mathiusso@gmail.com',
      password: '123456',
    })

    expect(response.status).toBe(400)
  })

  it('should not create user if the e-mail is already in use', async () => {
    await factory.create('User', {
      email: 'dev.mathiusso@gmail.com',
    })

    const response = await request(app).post(`${routePrefix}/register`).send({
      name: 'Victor Mathiusso',
      email: 'dev.mathiusso@gmail.com',
      password: '123456',
    })

    expect(response.status).toBe(409)
  })

  it('should authenticate with valid credentials', async () => {
    const user = await factory.create('User', { password: '123456' })

    const response = await request(app).post(`${routePrefix}/login`).send({
      email: user.email,
      password: '123456',
    })

    expect(response.status).toBe(200)
    expect(response.body).toHaveProperty('user')
    expect(response.body).toHaveProperty('token')
  })

  it('should not authenticate with invalid credentials', async () => {
    const user = await factory.create('User', { password: '654321' })

    const response = await request(app).post(`${routePrefix}/login`).send({
      email: user.email,
      password: '123456',
    })

    expect(response.status).toBe(401)
  })

  it('should not authenticate if the user was not found by e-mail', async () => {
    const response = await request(app).post(`${routePrefix}/login`).send({
      email: 'dev.victor@gmail.com',
      password: '123456',
    })

    expect(response.status).toBe(401)
  })

  it('should return JWT token when authenticated successfully', async () => {
    const user = await factory.create('User', { password: '123456' })

    const response = await request(app).post(`${routePrefix}/login`).send({
      email: user.email,
      password: '123456',
    })

    expect(response.status).toBe(200)
    expect(response.body).toHaveProperty('token')
  })

  it('should be able to access private routes when authenticated', async () => {
    const user = await factory.create('User')

    const response = await request(app)
      .get('/api/v1/categories')
      .set('Authorization', `Bearer ${user.generateToken()}`)

    expect(response.status).toBe(200)
  })

  it('should not be able to access private routes with invalid JWT token', async () => {
    const response = await request(app)
      .get('/api/v1/categories')
      .set('Authorization', `Bearer 123123`)
    expect(response.status).toBe(401)
  })
})

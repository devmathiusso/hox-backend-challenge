require('dotenv').config({
  path: process.env.NODE_ENV === 'test' ? '.env.test' : '.env',
})

// Middlewares
const xss = require('xss-clean')
const helmet = require('helmet')
const express = require('express')

// Routes
const authRoutes = require('./app/routes/auth')
const categoryRoutes = require('./app/routes/category')
const productRoutes = require('./app/routes/product')

// Errors Handlers
const AppError = require('./app/utils/appError')
const globalErrorHandler = require('./app/controllers/ErrorController')

class AppController {
  constructor() {
    this.express = express()

    this.middlewares()
    this.routes()
  }

  middlewares() {
    // Set security HTTP headers
    this.express.use(helmet())

    // Data sanitization against XSS
    this.express.use(xss())

    // Allow requests in JSON format
    this.express.use(express.json())
  }

  routes() {
    this.express.use('/api/v1/auth', authRoutes)
    this.express.use('/api/v1/categories', categoryRoutes)
    this.express.use('/api/v1/products', productRoutes)

    this.express.all('*', (req, res, next) => {
      next(new AppError(`Can't find ${req.originalUrl} on this server!`, 404))
    })

    this.express.use(globalErrorHandler)
  }
}

module.exports = new AppController().express

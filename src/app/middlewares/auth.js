const jwt = require('jsonwebtoken')
const { promisify } = require('util')
const AppError = require('../utils/appError')
const catchAsync = require('../utils/catchAsync')

module.exports = catchAsync(async (req, res, next) => {
  const { authorization } = req.headers

  if (!authorization) {
    return next(new AppError('Token not provided', 401))
  }

  const [, token] = authorization.split(' ')

  try {
    // Verify if the given token is valid
    const decoded = await promisify(jwt.verify)(token, process.env.APP_SECRET)
    req.userId = decoded.id
    return next()
  } catch (err) {
    return next(new AppError('Invalid token', 401))
  }
})

module.exports = (sequelize, DataTypes) => {
  const Product = sequelize.define('Product', {
    name: DataTypes.STRING,
    manufacturing_date: DataTypes.DATE,
    perishable_product: DataTypes.BOOLEAN,
    expiration_date: DataTypes.DATE,
    price: DataTypes.DECIMAL(10, 2),
  })

  Product.associate = function (models) {
    this.belongsTo(models.Category, {
      foreignKey: 'category_id',
      as: 'category',
    })
  }

  return Product
}

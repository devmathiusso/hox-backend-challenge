module.exports = (sequelize, DataTypes) => {
  const Category = sequelize.define('Category', {
    name: DataTypes.STRING,
  })

  Category.associate = function (models) {
    this.hasMany(models.Product, {
      foreignKey: 'category_id',
      as: 'products',
    })
  }

  return Category
}

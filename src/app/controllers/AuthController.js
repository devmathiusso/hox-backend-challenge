const { User } = require('../models')
const AppError = require('../utils/appError')
const catchAsync = require('../utils/catchAsync')

exports.store = catchAsync(async (req, res, next) => {
  const { name, email, password } = req.body

  // If there's some missing fields, returns an error
  if (!name || !email || !password) {
    throw new AppError('There are fields missing in request', 400)
  }

  const userExists = await User.findOne({ where: { email } })

  // If there's an user using the given e-mail, returns an error
  if (userExists) {
    throw new AppError('This e-mail is already in use', 409)
  }

  const user = await User.create({ name, email, password })

  return res.status(201).json({ user })
})

exports.login = catchAsync(async (req, res, next) => {
  const { email, password } = req.body

  const user = await User.findOne({ where: { email } })

  // If there's no user with the provided e-mail, returns an error
  if (!user) {
    throw new AppError('User not found', 401)
  }

  // Compare the provided password with the user password, if it's incorrect returns an error
  if (!(await user.checkPassword(password))) {
    throw new AppError('Incorrect password', 401)
  }

  return res.json({
    user,
    token: user.generateToken(),
  })
})

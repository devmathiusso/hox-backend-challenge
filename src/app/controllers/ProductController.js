const { Product } = require('../models')
const { validateRequest } = require('../helpers/products')
const AppError = require('../utils/appError')
const catchAsync = require('../utils/catchAsync')
const ApiFeatures = require('../utils/apiFeatures')

exports.index = catchAsync(async (req, res, next) => {
  const { page, limit } = req.query

  const paginationPage = page * 1 || 1
  const paginationLimit = limit * 1 || 10

  const products = await new ApiFeatures(Product, req.query)
    .filter()
    .sortResults()
    .paginate()
    .getResults()

  return res.json({
    page: paginationPage,
    items_per_page: paginationLimit,
    products,
  })
})

exports.show = catchAsync(async (req, res, next) => {
  const { id } = req.params

  const product = await Product.findByPk(id, {
    include: { association: 'category' },
  })

  if (!product) {
    throw new AppError('Product not found', 404)
  }

  res.json({ product })
})

exports.store = catchAsync(async (req, res, next) => {
  const productObj = await validateRequest(req.body)
  const product = await Product.create(productObj)
  return res.status(201).json({ product })
})

exports.update = catchAsync(async (req, res, next) => {
  const { id } = req.params

  const product = await Product.findByPk(id)

  if (!product) {
    throw new AppError('Product not found', 404)
  }

  const productObj = await validateRequest(req.body)

  product.category_id = productObj.category_id
  product.name = productObj.name
  product.manufacturing_date = productObj.manufacturing_date
  product.perishable_product = productObj.perishable_product
  product.expiration_date = productObj.expiration_date
  product.price = productObj.price
  await product.save()

  return res.status(200).json({ product })
})

exports.delete = catchAsync(async (req, res, next) => {
  const { id } = req.params

  // If the product was not found and deleted due to incorrect id, returns an error
  if (!(await Product.destroy({ where: { id } }))) {
    throw new AppError('Product not found', 404)
  }

  return res.status(204).send()
})

const { Category } = require('../models')
const { validateRequest } = require('../helpers/categories')
const AppError = require('../utils/appError')
const catchAsync = require('../utils/catchAsync')
const ApiFeatures = require('../utils/apiFeatures')

exports.index = catchAsync(async (req, res, next) => {
  const { page, limit } = req.query

  const paginationPage = page * 1 || 1
  const paginationLimit = limit * 1 || 10

  const categories = await new ApiFeatures(Category, req.query)
    .filter()
    .sortResults()
    .paginate()
    .getResults()

  return res.json({
    page: paginationPage,
    items_per_page: paginationLimit,
    categories,
  })
})

exports.show = catchAsync(async (req, res, next) => {
  const { id } = req.params

  const category = await Category.findByPk(id, {
    include: { association: 'products' },
  })

  if (!category) {
    throw new AppError('Category not found', 404)
  }

  res.json({ category })
})

exports.store = catchAsync(async (req, res, next) => {
  const categoryObj = await validateRequest(req.body, res)
  const category = await Category.create(categoryObj)
  return res.status(201).json({ category })
})

exports.update = catchAsync(async (req, res, next) => {
  const { id } = req.params

  const category = await Category.findByPk(id)

  if (!category) {
    throw new AppError('Category not found', 404)
  }

  const categoryObj = await validateRequest(req.body, res)

  category.name = categoryObj.name
  await category.save()

  return res.status(200).json({ category })
})

exports.delete = catchAsync(async (req, res, next) => {
  const { id } = req.params

  // If the category was not found and deleted due to incorrect id, returns an error
  if (!(await Category.destroy({ where: { id } }))) {
    throw new AppError('Category not found', 404)
  }

  res.status(204).send()
})

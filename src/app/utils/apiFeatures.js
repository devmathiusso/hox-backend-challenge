const { Op } = require('sequelize')

class ApiFeatures {
  constructor(model, queryString) {
    this.model = model
    this.queryParams = {}
    this.queryString = queryString
  }

  filter() {
    const queryObj = { ...this.queryString }

    if (queryObj.name) {
      this.queryParams.where = {
        name: { [Op.iLike]: `%${queryObj.name}%` },
      }
    }

    if (queryObj.categories) {
      const categoriesInt = queryObj.categories
        .split(',')
        .map((categoryId) => parseInt(categoryId))

      this.queryParams.where = {
        ...this.queryParams.where,
        category_id: [...categoriesInt],
      }
    }

    return this
  }

  sortResults() {
    const queryObj = { ...this.queryString }
    const { order_by } = queryObj

    const orderBy = order_by || 'id,desc'

    this.queryParams.order = [orderBy.split(',')]

    return this
  }

  paginate() {
    const queryObj = { ...this.queryString }
    const { page, limit } = queryObj

    const paginationPage = page * 1 || 1
    const paginationLimit = limit * 1 || 10
    const offset = (paginationPage - 1) * paginationLimit

    this.queryParams.offset = offset
    this.queryParams.limit = paginationLimit

    return this
  }

  async getResults() {
    return await this.model.findAll(this.queryParams)
  }
}

module.exports = ApiFeatures

/**
 * Utility function created to wrap an async/await function and to avoid the
 * try/catch block repeating everytime
 */
module.exports = (fn) => {
  return (req, res, next) => {
    fn(req, res, next).catch(next)
  }
}

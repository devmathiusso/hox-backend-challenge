const routes = require('express').Router()
const authMiddleware = require('../middlewares/auth')
const ProductController = require('../controllers/ProductController')

routes.use(authMiddleware)

routes.route('/').get(ProductController.index).post(ProductController.store)

routes
  .route('/:id')
  .get(ProductController.show)
  .patch(ProductController.update)
  .delete(ProductController.delete)

module.exports = routes

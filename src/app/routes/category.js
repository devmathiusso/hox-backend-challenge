const routes = require('express').Router()
const authMiddleware = require('../middlewares/auth')
const CategoryController = require('../controllers/CategoryController')

routes.use(authMiddleware)

routes.route('/').get(CategoryController.index).post(CategoryController.store)

routes
  .route('/:id')
  .get(CategoryController.show)
  .patch(CategoryController.update)
  .delete(CategoryController.delete)

module.exports = routes

const routes = require('express').Router()
const AuthController = require('../controllers/AuthController')

routes.post('/login', AuthController.login)
routes.post('/register', AuthController.store)

module.exports = routes

const AppError = require('../utils/appError')

const validateRequest = async (reqBody) => {
  const { name } = reqBody

  // If there's some missing fields, returns an error
  if (!name) {
    throw new AppError('There are fields missing in request', 400)
  }

  return { name }
}

module.exports = { validateRequest }

const { Category } = require('../models')
const AppError = require('../utils/appError')

const validateRequest = async (reqBody) => {
  const {
    category_id,
    name,
    manufacturing_date,
    perishable_product,
    expiration_date,
    price,
  } = reqBody

  // If there's some missing fields, returns an error
  if (
    !category_id ||
    !name ||
    !manufacturing_date ||
    perishable_product === undefined ||
    !expiration_date ||
    !price
  ) {
    throw new AppError('There are fields missing in request', 400)
  }

  const category = await Category.findByPk(category_id)

  if (!category) {
    throw new AppError('Category not found', 404)
  }

  // If the manufacturing date is an invalid date, returns an erroe
  if (isNaN(Date.parse(manufacturing_date))) {
    throw new AppError('The manufacturing date is not a valid format', 400)
  }

  // If the expiration date is an invalid date, returns an erroe
  if (isNaN(Date.parse(expiration_date))) {
    throw new AppError('The expiration date is not a valid format', 400)
  }

  // If the manufacturing date is greater than the expiration date, returns an error
  if (new Date(manufacturing_date) > new Date(expiration_date)) {
    throw new AppError(
      'The manufacturing date should not be greather than the expiration date',
      400
    )
  }

  return {
    category_id,
    name,
    manufacturing_date,
    perishable_product,
    expiration_date,
    price,
  }
}

module.exports = { validateRequest }
